#!/usr/bin/env ruby
require './image_exceptions'

# This class defines an image. Images are represented as an M x N array of pixels with each pixel given colour.
class Image
  include ExceptionForImage

  # @return [Numeric]
  attr_reader :width
  # @return [Numeric]
  attr_reader :height
  # @return [Image]
  attr_accessor :image

  # Creates an Image
  def initialize(w, h)
    return Image.Raise ErrArgType unless (w.instance_of? Fixnum) || (h.instance_of? Fixnum) # Integer for ruby >=2.4
    return Image.Raise ErrImageDimensionMismatch if w < 1 || w > 250 || h < 1 || h > 250
    @width =  w
    @height = h
    @image = Array.new(h) { Array.new(w, 'O') }
  end

  # Resets image filling all pixels with 'O'
  # @return [Image] the object
  def clear
    self.image = Array.new(height) { Array.new(width, 'O') }
  end

  # Colours the pixel (x,y) with colour c.
  # @param x [Numeric] pixel column
  # @param y [Numeric] pixel row
  # @param c [String] pixel color
  # @return [Image] the object
  def color_pixel(x, y, c)
    # TODO: pixels could also be Pixel objects useful for step fill region (MST?)
    raise ErrPixelOutOfBounds if x > width || y > height
    raise ErrPixelOutOfBounds if x < 1 || y < 1
    image[y - 1][x - 1] = c.upcase
    image
  end

  # Draws a vertical segment of colour C in column X between rows Y1 and Y2 (inclusive).
  # @param x [Numeric] column
  # @param y1 [Numeric] row start
  # @param y2 [Numeric] row end
  # @param c [String] color
  # @return [Image] the object
  def color_column(x, y1, y2, c)
    raise ErrColumnOutOfBounds unless x <= width
    raise ErrColumnOutOfBounds unless y1 > 0 && y1 <= height
    raise ErrColumnOutOfBounds unless y2 > 0 && y2 <= height

    image[y1 - 1..y2 - 1].each do |row|
      row[x - 1] = c.upcase
    end
    image
  end

  # Draws a horizontal segment of colour C in row y between columns X1 and X2 (inclusive)
  # @param x1 [Numeric] row start
  # @param x2 [Numeric] row end
  # @param y [Numeric] column
  # @param c [String] color
  # @return [Image] the object
  def color_row(x1, x2, y, c)
    raise ErrRowOutOfBounds unless y <= height
    raise ErrColumnOutOfBounds unless x1 > 0 && x1 <= width
    raise ErrColumnOutOfBounds unless x2 > 0 && x2 <= width &&
    from = x1 -1
    to = x2 - 1
    row = image[y - 1]
    (from..to).each { |e| row[e] = c.upcase }
    image
  end

  # Image to String
  # @return [String]
  def to_s
    s = []
    image.each { |row| s << row.join }
    s.join("\n")
  end

  private

  # def check_from(from)
  #   from < 1 ? 0 : from - 1
  # end

  # def check_to(to)
  #   to > width ? width : to - 1
  # end
end
