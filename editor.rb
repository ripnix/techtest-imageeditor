#!/usr/bin/env ruby

require './image'
require 'highline'

cli = HighLine.new
help = <<HELP
  The editor supports 7 commands:
  1. I M N. Create a new M x N image with all pixels coloured white (O).
  2. C. Clears the table, setting all pixels to white (O).
  3. L X Y C. Colours the pixel (X,Y) with colour C.
  4. V X Y1 Y2 C. Draw a vertical segment of colour C in column X between rows Y1 and Y2 (inclusive).
  5. H X1 X2 Y C. Draw a horizontal segment of colour C in row Y between columns X1 and X2 (inclusive).
  6. F X Y C. Fill the region R with the colour C. R is defined as: Pixel (X,Y) belongs to R. Any other pixel which is the same colour as (X,Y) and shares a common side with any pixel in R also belongs to this region.
  7. S. Show the contents of the current image
  8. X. Terminate the session
  9. HELP. Show this help
HELP

answer = cli.ask("<%= color('Please create an image using I M N', BOLD) %>!") { |q| q.validate = /I\s+\d+\s+\d+/i }.split
i = Image.new(answer[1].to_i,answer[2].to_i)
puts i
puts help
loop do
  input = gets.chomp
  command, *params = input.split /\s/
  case command
  when /\AC\z/i
    i.clear
  when /\AL\z/i
    i.color_pixel(params[0].to_i,params[1].to_i, params[2].upcase!) if params.count == 3 rescue ExceptionForImage::ErrPixelOutOfBounds
    puts i
  when /\AV\z/i
    i.color_column(params[0].to_i,params[1].to_i, params[2].to_i, params[3].upcase!) if params.count == 4 rescue ExceptionForImage::ErrColumnOutOfBounds
    puts i
  when /\AH\z/i
    i.color_row(params[0].to_i,params[1].to_i, params[2].to_i, params[3].upcase!) if params.count == 4 rescue ExceptionForImage::ErrRowOutOfBounds
    puts i
  when /\AS\z/i
    puts i
  when /\AX\z/i
    cli.say("<%= color('Thank you. Good bye!', BOLD) %>!")
    break
  when /help/i
    cli.say(help)
  else puts 'Invalid command'
  end
end
