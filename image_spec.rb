# frozen_string_literal: false
require 'minitest/autorun'
require 'minitest/spec'
require './image'

class SpecImage < Minitest::Test
  describe Image do
    it 'can be created with no color arguments' do
      image.must_be_instance_of Image
    end
    it "can't create image without numeric" do
      err = -> { Image.new('width', 'height') }.must_raise ExceptionForImage::ErrArgType
      err.message.must_match /Must be non negative integer/
    end
    it "can't create image 0,0" do
      err = -> { Image.new(0, 0) }.must_raise ExceptionForImage::ErrImageDimensionMismatch
      err.message.must_match /Image dimension mismatch/
    end
    it "can't create image > I(251,251)" do
      err = -> { Image.new(251, 251) }.must_raise ExceptionForImage::ErrImageDimensionMismatch
      err.message.must_match /Image dimension mismatch/
    end

    let(:image) { Image.new(5, 6) }

    it 'can color a pixel' do
      image.color_pixel(2, 3, 'a').must_equal [%w(O O O O O), %w(O O O O O), %w(O A O O O), %w(O O O O O), %w(O O O O O), %w(O O O O O)]
    end

    it 'can color on a row' do
      image.color_row(3, 4, 2, 'Z').must_equal [%w(O O O O O), %w(O O Z Z O), %w(O O O O O), %w(O O O O O), %w(O O O O O), %w(O O O O O)]
    end

    it 'can color on a column' do
      image.color_column(2, 3, 4, 'W').must_equal [%w(O O O O O), %w(O O O O O), %w(O W O O O), %w(O W O O O), %w(O O O O O), %w(O O O O O)]
    end

    it 'can clear an image' do
      image.color_column(5, 1, 6, 'W').must_equal [%w(O O O O W), %w(O O O O W), %w(O O O O W), %w(O O O O W), %w(O O O O W), %w(O O O O W)]
      image.clear.must_equal [%w(O O O O O), %w(O O O O O), %w(O O O O O), %w(O O O O O), %w(O O O O O), %w(O O O O O)]
    end

    it "can't color pixel out of image boundaries" do
      -> { image.color_pixel(10, 6, 'V') }.must_raise ExceptionForImage::ErrPixelOutOfBounds
      -> { image.color_pixel(2, 10, 'V') }.must_raise ExceptionForImage::ErrPixelOutOfBounds
      -> { image.color_pixel(0, 0, 'V') }.must_raise ExceptionForImage::ErrPixelOutOfBounds
    end

    it "can't color column out of image boundaries" do
      -> { image.color_column(7, 3, 4, 'W') }.must_raise ExceptionForImage::ErrColumnOutOfBounds
    end

    it "can't color row out of image boundaries" do
      -> { image.color_row(3, 4, 10, 'Z') }.must_raise ExceptionForImage::ErrRowOutOfBounds
    end
  end
end
