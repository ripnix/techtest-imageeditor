# frozen_string_literal: false
# require 'test/unit'
require 'minitest/autorun'

require './image'

class TestImage < Minitest::Test
  def setup
    @i1 = Image.new(5, 6)
    # @bad_image1 = Image.new(0,0)
  end

  def test_image1
    assert_equal 5, @i1.width
    assert_equal 6, @i1.height
    assert_equal [%w(O O O O O), %w(O O O O O), %w(O O O O O), %w(O O O O O), %w(O O O O O), %w(O O O O O)], @i1.image
    assert_equal [%w(O O O O O), %w(O O O O O), %w(O A O O O), %w(O O O O O), %w(O O O O O), %w(O O O O O)], @i1.color_pixel(2, 3, 'A')
    assert_equal [%w(O O O O O), %w(O O O O O), %w(O W O O O), %w(O W O O O), %w(O O O O O), %w(O O O O O)], @i1.color_column(2, 3, 4, 'W')
    assert_equal [%w(O O O O O), %w(O O Z Z O), %w(O W O O O), %w(O W O O O), %w(O O O O O), %w(O O O O O)], @i1.color_row(3, 4, 2, 'Z')
    assert_equal [%w(O O O O O), %w(O O O O O), %w(O O O O O), %w(O O O O O), %w(O O O O O), %w(O O O O O)], @i1.clear
  end
end
