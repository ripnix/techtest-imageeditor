# frozen_string_literal: false
require 'e2mmap.rb'

module ExceptionForImage # :nodoc:
  extend Exception2MessageMapper

  def_e2message(ArgumentError, 'Wrong # of arguments(%d for %d)')
  def_exception('ErrArgType', 'Wrong type arguments. Must be non negative integer')
  def_exception('ErrImageDimensionMismatch', "\#{self.name} dimension mismatch: Image(M x N): 1 <= M, N <= 250 ")
  def_exception('ErrRowOutOfBounds', "Row is bigger than \#{self.height}")
  def_exception('ErrColumnOutOfBounds', "Column is bigger than \#{self.width}")
  def_exception('ErrPixelOutOfBounds', "Selected pixel \#{self[y][x]} is out of bounds")
end
